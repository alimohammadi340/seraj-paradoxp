<?php
/**
 * Created by PhpStorm.
 * User: mphj
 * Date: 11/2/2017
 * Time: 10:52 AM
 */
namespace App;
class Uploader
{
    const TYPE_IMAGE = 0, TYPE_RAW = 1, NAME_WITHOUT_EXT = 0, NAME_WITH_EXT = 1;
    private $file;
    private $type;

    public static function fromInput($inputName){
        if (!isset($_FILES[$inputName]))
            throw new Exception("Undefined file $inputName");
        $uploader = new Uploader();
        $uploader->file = $_FILES[$inputName];
        return $uploader;
    }


    public function asImage(){
        $this->type = self::TYPE_IMAGE;
        return $this;
    }


    public function asRawFile(){
        $this->type = self::TYPE_RAW;
        return $this;
    }


    public function validate(){
        switch ($this->type){
            case self::TYPE_IMAGE:
                if ($this->fileExists() && $this->isValidImage())
                    return $this;
                throw new \Exception("Invalidate");
                break;
            case self::TYPE_RAW:
                if ($this->fileExists())
                    return $this;
                throw new \Exception("Invalidate");
                break;
            default:
                throw new Exception("File type not defined");
        }
        return $this;
    }


    public function into($destinationPath, $fileName, $fileNameType = Uploader::NAME_WITHOUT_EXT){
        $destinationPath = $this->getDirWithTypeAndFileName($destinationPath, $fileName, $fileNameType);
        return move_uploaded_file($this->getTmpName(), $destinationPath);
    }


    public function getDirWithTypeAndFileName($dir, $fileName, $nameType){
        switch ($nameType) {
            case self::NAME_WITHOUT_EXT:
                return $dir . $fileName . '.' . $this->getExtension();
                break;
            case self::NAME_WITH_EXT:
                return $dir . $fileName;
                break;
            default:
                throw new Exception("Undefined type");
        }
    }


    public function fileExists(){
        return file_exists($this->getTmpName());
    }


    public function getTmpName(){
        if ($this->file == null)
            throw new Exception("Undefined file");
        return $this->file['tmp_name'];
    }

    public function getNameWithoutExtension(){
        return pathinfo($this->getName())['filename'];
    }

    public function getExtension(){
        return pathinfo($this->getName())['extension'];
    }


    public function getName(){
        return $this->file['name'];
    }

    public function isValidImage(){
        if ($this->fileExists()){
            return getimagesize($this->getTmpName()) !== FALSE;
        }
        return false;
    }
}