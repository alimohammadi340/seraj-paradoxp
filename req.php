<?php

include 'includes/core.php';

if(isset($_POST['req'])){
    $req = $_POST['req'];
    include "requests/$req.php";
} else {
    header('Location: ' . $config['address']);
}